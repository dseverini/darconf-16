import java.util.*;

public class Main {
    public static void main(String[] args) {
        //dada una matriz de nxm y una pieza de jxk cuáles son la cantidad mínima de movimientos que me asegura encontrar la pieza
        //En el  peor caso la pieza estará al final de la matriz. Rellenamos con 0 los espacios libres y con 1 dónde está la pieza
        System.out.println("Hipótesis: La cantidad mínima de movimientos para encontrar la pieza es [n/j].[m/k]");
        System.out.println("donde n es la cantidad de filas del tablero, m es la cantidad de columnas del tablero y j y k los lados de la pieza (cuadrada).");
        System.out.println("Es decir, la cantidad mínima de movimientos para encontrar la pieza se encuentra al multiplicar las partes enteras de las " +
                "divisiones n/j y m/k");
        System.out.println("");
        System.out.println("Partimos al llenar el tablero con 0 en los espacios no recorridos sin pieza en ellos, un 1 en los espacios dónde se encuentre" +
                " la pieza");
        System.out.println("y :( en los espacios recorridos dónde no está la pieza");
        boolean continua = true;
        Scanner scan = new Scanner(System.in);
        while (continua) {
            System.out.println("");
            System.out.println("********************************************************************************************************************************");
            System.out.println("Ingresa el número de filas del tablero (n)");
            int matrixRow = scan.nextInt(); //n

            System.out.println("Ingresa el número de columnas del tablero (m)");
            int matrixCol = scan.nextInt(); //m
            boolean valid = false;
            int pieceRows = 1;
            while (!valid) {
                System.out.println("Ingresa un número válido para el tamaño de la pieza (j=k por ser cuadrada)");
                pieceRows = scan.nextInt(); //j = k
                valid = (pieceRows <= matrixRow) && (pieceRows <= matrixCol) && (pieceRows > 0);
                System.out.println(valid);
            }
            System.out.println(pieceRows);
            //defining 2D array to hold matrix data
            String[][] matrix = new String[matrixRow][matrixCol];
            // Enter Matrix Data
            enterMatrixData(matrix, matrixRow, matrixCol, pieceRows);

            // Find piece
            int turnos = findPiece(matrix, matrixRow, matrixCol, pieceRows);
            printMatrix(matrix, matrixRow, matrixCol);
            System.out.println("Pieza encontrada en " +turnos + " turnos.");
            System.out.println("La cantidad minima de movimientos según la hipótesis [n/j].[m/k] = " + (matrixRow/pieceRows) * (matrixCol/pieceRows));
            System.out.println("");
            System.out.println("Desea continuar? s/n");
            String respuesta = scan.next();
            continua =  respuesta.equals("s") ? true : false;
        }
    }

    public static void enterMatrixData(String[][] matrix, int matrixRow, int matrixCol, int pieceRow) {
        for (int i = 0; i < matrixRow; i++) {
            for (int j = 0; j < matrixCol; j++) {
                matrix[i][j] = "0";
            }
        }

        for (int i = 1; i <= pieceRow; i++) {
            for (int j = 1; j <= pieceRow; j++) {
                matrix[matrixRow - i][matrixCol - j] = "1";
            }
        }
    }

    public static void printMatrix(String[][] matrix, int matrixRow, int matrixCol) {
        System.out.println("Tu tablero es : ");

        for (int i = 0; i < matrixRow; i++) {
            for (int j = 0; j < matrixCol; j++) {
                System.out.print(matrix[i][j] + "\t");
            }

            System.out.println();
            System.out.println();
        }

        System.out.println("Referencia: 0 espacio vacío, 1 ocupado por pieza, :( verificado sin pieza");
    }

    public static int findPiece(String[][] matrix, int matrixRow, int matrixCol, int pieceRow) {
        boolean encontrada = false;
        int registrandoFila = 0;
        int registrandoColumna = 0;

        if (pieceRow > 1) {
            registrandoFila = pieceRow - 1;
            registrandoColumna = pieceRow - 1;
        }
        int contadorDeTurnos = 0;
        int j;
        while (registrandoFila < matrixRow && !encontrada) {
            j = registrandoColumna;
            while (j < matrixCol && !encontrada) {
                contadorDeTurnos ++;
                if (piezaEncontrada(matrix[registrandoFila][j])) {
                    encontrada = true;
                } else {
                    matrix[registrandoFila][j] = ":(";
                    j += pieceRow;
                }
            }
            registrandoFila += pieceRow;
        }
        return contadorDeTurnos;
    }

    private static boolean piezaEncontrada(String matrix) {
        return matrix == "1";
    }
}
