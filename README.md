# darconf-16
# consigna
El episodio en cuestión, nos encontrará tratando de encontrar una pieza escondida en un tablero. Seguramente, todos somos capaces de diseñar un algoritmo, que dado un tablero de 8x8, encuentre una pieza de 2x2 escondida. Ahora, que pasa si el tablero es NxM y la pieza KxJ? Nuevamente, casi con certeza, lograremos implementar el algoritmo para solucionar el problema. 
   
Pero, probablemente, a algunos nos empezará a picar la curiosidad: ¿Cúal es el mejor de todos los posibles algoritmos? Y, ¿hay alguna fórmula única que me anticipe el numero de movimientos que el algoritmo requerirá para distintos tableros y piezas? ¿Podré compartir una solución con un marco teórico en stackoverflow o por ahora solo seguiré buscando respuestas allí?

Es así, que durante esta primera experiencia, descubriremos paso a paso el trabajo para encontrar la solución a la siguiente pregunta:
Dado un tablero de NxM y una pieza rectangular de KxJ escondida en el, y utilizando un sistema de turnos en el cual cada "turno" puedo descubrir 1 cuadrado del tablero, 
¿Cúal es el número MÍNIMO de turnos que necesita un algoritmo para asegurarnos la posición de la pieza escondida, sin depender de encontrarla azarosamente?

# Hipótesis
La hipótesis se encuentra en este proyecto junto con la prueba de la misma mediante la experimentación con el usuario. 
El usuario podrá configurar diferentes tableros y piezas y el algoritmo, utilizando el peor escenario en el cuál se podría encontrar la pieza llevando la 
mayor cantidad de movimientos posibles, encontrará la pieza y contará la cantidad de movimientos que llevó hacerlo.

# Uso de proyecto
Puede importarlo en su IDE como proyecto Maven o también
mvn clean install && mvn exec:java